---
title: "Vision"
date: 2019-04-16T16:08:47+02:00
layout: "single"
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
#header_wrapper_class: ""
#seo_title: ""
#headline: ""
#subtitle: ""
#tagline: ""
#links: []
---

The OpenMobility interest group will drive the evolution and broad adoption of mobility modelling and simulation technologies.
It accomplishes its goals by fostering and leveraging collaborations among members and by ensuring the development and availability of necessary tools for a detailed simulation of the mobility patterns of people and vehicles.
It will be valuable for cities, municipalities, public transport providers as well as automotive software developers by offering the capability to create digital twins, optimize traffic systems and evaluate new business concepts, such as Mobility-as-a-Service.

It is the goal of this group to deliver a framework of tools based on validated models, which are accepted as "standard tools" in industry applications and academic research. 
The interest group will:

- Coordinate the development of related Eclipse Foundation projects towards an openMobility framework.
- Define and manage the specifications for interfaces and functions for the framework.
- Promote the openMobility framework and its value.
- Provide a vendor neutral marketing and other services to the openMobility ecosystem.
- Define licensing and intellectual property flows that encourage community participation and tool adoption.
- Drive the funding model that enables this group and its community to operate on a sustainable basis.