---
title: "openMobility Telco Feb 18, 2025"
date: 2025-02-18T17:09:45+02:00
layout: "single"
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
---

## Attendees

- Robert Hilbrich (DLR, co4e GmbH)
- Michael Behrisch (DLR)
- Karl Schrab (FhG FOKUS)
- Daniel Wesemeyer (DLR)
- Robert Protzmann (FhG FOKUS)
- Jakob Kaths (Vector)
- Andy Riexinger (Bosch)
- Angelo Banse (DLR)
- Herve
- Jakob Erdmann (DLR)
- Josko Balic (AVL)
- Ken Horne
- Lutz Kelch
- Mirko Barthauer (DLR)
- Peter Wagner (DLR)
- Philipp Heisig (FH Dortmund)
- Philipp Rosenberger (Persival)
- Ronald Nippold (DLR)
- Moritz Schweppenhäuser (FhG FOKUS)
- Simon Terres (AVL)
- Yun-Pang Flötteröd (DLR)

## News

### Eclipse SUMO

- Release 1.22.0 has been released
  - Rework on the railway code to make it faster
  - Stationfinder device improved
  - New installer for macOS (signed!)
  - Old Linux versions deprecated (e.g. CentOS7)
- SUMO User Conference 2025 - deadline for paper and poster submissions is approaching fast (Sunday!)

### Eclipse MOSAIC

- New workshop and teaching material for Eclipse MOSAIC - in addition to the homepage
- Submission for the SUMO User Conference in the works
- Synthetic LIDAR dataset published based on SUMO, MOSAIC and CARLA

### Eclipse openMCx

- see Tech Demo later on

### Other Community News

- Welcome to Transcality as a new member of the openMobility interest group

## Tech Demo: Eclipse MOSAIC by FhG FOKUS

- MOSAIC as a co-simulation framework is presented by Karl Schrab
- Example Use-Case "Collective Perception"
- Hands-on Workshops planned in March 2025 and as part of the SUMO User Conference 2025

## Tech Demo: Sensor-Simulation Tools by PERSIVAL

- Credible Sensor Simulation Tools presented by Philipp Rosenberger

## Any other Business

- None
