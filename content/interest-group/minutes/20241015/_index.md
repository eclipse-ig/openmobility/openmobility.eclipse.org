---
title: "openMobility Telco Oct 15, 2024"
date: 2024-10-15T17:09:45+02:00
layout: "single"
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
---

## Attendees

- Robert Hilbrich (DLR & co4e GmbH)
- Michael Behrisch (DLR)
- Benjamin Coueraud (DLR)
- Angelo Banse (DLR)
- Jakob Erdmann (DLR)
- Robert Protzmann (FhG FOKUS)
- Karl Schrab (FhG FOKUS)
- Michele Segata
- Philipp Heisig (FH Dortmund)
- Josko Balic (AVL)

## News

### Eclipse SUMO

- Latest release v1.21.0 of SUMO is available - [release notes](https://sumo.dlr.de/docs/ChangeLog.html#version_1210_10102024)
- Ongoing work towards calibration: finding optimal parameters for vehicle types to match observed behavior on motorways
- Ongoing work on a redesigned process to build the python wheels by using [cibuildwheel](https://cibuildwheel.pypa.io/en/1.x/), no more Python 2
- Ongoing work towards a signed and notarized macOS installer: [issue #15213](https://github.com/eclipse-sumo/sumo/issues/15213)
- Save the date for the SUMO User Conference 2025: **May 12-14 2025 in Berlin** - more information: [https://eclipse.dev/sumo/conference/](https://eclipse.dev/sumo/conference/)
- We plan to have the Conference proceedings SCOPUS indexed

### Eclipse MOSAIC

- New release of MOSAIC 2024.1 with support of the latest SUMO release
- Most work was in the maven build and documentation area
- Currently a lot of workshops; maybe 1h hands-on workshop as part of the SUMO User Conference 2025
- Next steps: focus on usability and workshops, also a new ambassador in the works

### Eclipse openMCx

- Implementaton of DCP will be merged shortly in the main branch
- EU Project VALID tested models of openMCx, which lead to DCP implementation
- FMI 3.0 is not spread as wide as hoped
- Company [Persival.de](https://Persival.de) uses openMCx (FMI 2.x) - work in ADAS world

### Other News

- Michele: pull requests for SUMO (platooning manoeuvers) has been added - but some side-cases are still not working
- Philipp: doing PhD for a cloud based mobility testing service

## Tech Demo: How to simulate pedestrians in SUMO?

- See pedestrian simulator [JuPedSim](https://www.jupedsim.org)
- See [CroMa-PRO project](https://www.croma-pro.de/de/forschung), [DLR news](https://www.dlr.de/de/aktuelles/nachrichten/2024/dlr-unterstuetzt-reibungslose-an-und-abreise-zur-em-2024) and [FZ Jülich news](https://www.fz-juelich.de/en/ias/ias-7/research-1/spotlight/safety-research-at-the-uefa-euro-2024-in-dusseldorf)

## EclipseCon24 - Open Community Experience (OCX)

- 22-24 October, Mainz (Germany) - [conference website](https://www.ocxconf.org)
- Michael and Angelo will be attending the EclipseCon 24 for the SUMO team
- Who is planning to attend?
- Who will give a talk?

## Any other Business

- DLR & co4e GmbH will be attending the [Smart Country Convention](https://www.smartcountry.berlin/) in Berlin tomorrow (Oct 16)
