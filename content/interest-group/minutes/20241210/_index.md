---
title: "openMobility Telco Dec 10, 2024"
date: 2024-12-10T17:09:45+02:00
layout: "single"
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
---

## Attendees

- Robert Hilbrich
- Michael Behrisch
- Benjamin Coueraud
- Jakob Erdmann
- Sasan Amini
- Andreas Riexinger
- Dominik Salles
- Timo Großmann
- Jakob Kaths
- Jay Kaplan
- Jérome Härri
- Josko Balic
- Karl Schrab
- Mirko Barthauer
- Moritz Schweppenhäuser
- Peter Wagner
- Philipp Heisig
- Yun-Pang Flötteröd

## News

### Eclipse SUMO

- We would greatly appreciate your support and would like to feature your organization's logo on the SUMO website
  - the SUMO team will contact you soon
- Ongoing work towards a signed and fully self contained macOS installer: [issue #15213](https://github.com/eclipse-sumo/sumo/issues/15213)
  - First version (unsigned) is available for testing here [https://github.com/eclipse-sumo/sumo/actions/runs/12248302337/artifacts/2297534955](https://github.com/eclipse-sumo/sumo/actions/runs/12248302337/artifacts/2297534955)
  - Please test and evaluate the installer
- macOS homebrew formula for SUMO will be deprecated
- Save the date for the SUMO User Conference 2025: **May 12-14 2025 in Berlin** - more information: [https://eclipse.dev/sumo/conference/](https://eclipse.dev/sumo/conference/)
  - We are still looking for reviewers (1-2 papers per reviewer) - please let us know, if you would like to support us
- We plan to have the Conference proceedings SCOPUS indexed
- Next release will be on Jan 20

### Eclipse MOSAIC

- Switch to Java17 - including Java17 features
- Improvement to send c2x messages
- Workshop roadmap work continues - there will be a free workshop at the end of February / March - in addition to the SUMO User Conference 2025
- Tech Demo in the next openMobility meetup

### Eclipse openMCx

- Company [https://www.persival.de/](https://www.persival.de/) presented their work with OpenMCx at an AVL company event previously
- OpenMCx DCP solution was used in the EU project VALID (about wave powerplants)
- Presentation could be done in one of the upcoming openMobility meetings

### Other News

- DLR is currently working towards
  - welcoming a new member organization to our community in 2025
  - adding two or three new Eclipse projects to our portfolio in 2025
  - submitting two EU research project proposals in 2025 which aim to disseminate the results, i.e., software tools, within the Eclipse Foundation
- SDV Hackathon (20.11.-22.11.) - there was one group who used SUMO - and they won 2nd price
  - Repository: [https://github.com/Eclipse-SDV-Hackathon-Chapter-Two/APT](https://github.com/Eclipse-SDV-Hackathon-Chapter-Two/APT)

## Tech Demo: Transcality

- Transcality scales the application of digital transportation twins 
- See [https://transcality.com/](https://transcality.com/)

## Research: Integration of Bus traffic in the SUMO Berlin scenario

- Report from the master thesis of Timo Großmann
- Berlin Sumo Traffic (BeST) Scenario - [https://github.com/mosaic-addons/best-scenario](https://github.com/mosaic-addons/best-scenario)

## Any other Business

- Merry Christmas and happy holidays!
