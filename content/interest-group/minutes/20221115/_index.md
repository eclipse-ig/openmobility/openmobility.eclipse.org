---
title: "openMobility Telco Nov 15th, 2022"
date: 2022-09-06T17:09:45+02:00
layout: "single"
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
---

## Attendees

- Robert Hilbrich (DLR)
- Michael Behrisch (DLR)
- Robert Protzmann (FhG FOKUS)
- Karl Schrab (FhG FOKUS)
- Moritz Schweppenhäuser (FhG FOKUS)
- Jakob Kaths (Vector)
- Andreas Riexinger (BOSCH)
- Michael Plagge (Eclipse Foundation)
- Sharon Corbett (Eclipse Foundation)
- Josko Balic (AVL)
- Bhanu (Goleyo)
- Dominik Salles (FKFS – Forschungsinstitut für Kraftfahrwesen und Fahrzeugmotoren Stuttgart)

## Introduction

## News from the Projects 

### Eclipse MOSAIC
- Berlin scenario was publicly released; see [Berlin Sumo Traffic Scenario](https://github.com/mosaic-addons/best-scenario) and last openMobility meeting minutes
- Moritz Schweppenhäuser is now part of the FhG FOKUS team working on Eclipse MOSAIC
- New release 22.1 of Eclipse MOSAIC in October 
  - Improvements of the perception models (realistic occlusion, improved ns3 coupling)

### Eclipse SUMO
- Last week: SUMO release 1.15.0 (see [Download page](https://sumo.dlr.de/docs/Downloads.php))
- Features:
  - improvements in traffic light editing in netedit
  - improvements of multimodal bicycle and pedestrian traffic handling
  - collection of scenarios in the GitHub repository - are there alternatives to the addition of the Berlin scenario as a submodule? --> FhG FOKUS will check for possible solutions
  - i18n basics for SUMO have been implemented - we use [Weblate](https://hosted.weblate.org/projects/eclipse-sumo/) to develop translations for Spanish and French for SUMO GUI and NetEdit

## Other News
- openADx Working Group will be terminated, effective Dec. 31, 2022
- EclipseCon
  - Automotive Community day: 
    - room was too small for the attendees; good feedback so far
  - Interest Groups were introduced in the members meeting at EclipseCon (and was part of the members newsletter)
  - Hackathon on the final day at EclipseCon was very well received
- Bosch Connected Experience (BCX)
  - Hackathon to combine Eclipse Projects in the SDV context
  - Andy achieved "winner of the hearts"

## Future of openMobility: Introduction of Interest Groups at Eclipse Foundation
- Sharon Corbett presents the concept of Interest Groups (see [Slides](../../../files/20221115-interest-groups.pdf))
- Motion: Robert Hilbrich moved to terminate the formal working group in favor of OpenMobility transitioning to an Eclipse Interest Group
  - Second: Andy Riexinger seconded the motion
  - No objections or absentions received
  - RESOLVED, the Steering Committee unanimously approves to terminate the formal OpenMobility working group and transition to a lighter weight Interest Group model based on the timeline presented during the meeting (transition complete end of January 2023).
- Robert Hilbrich was nominated as Interest Group lead for the openMobility interest group

## Any other Business
- Hackathon for "openMobility projects" to engage contributors / increase amount of committers for projects?
- Onboarding training / talk about "How to become a committer to an Open Source project"?
- Good idea - should be pursued in conjunction with Automotive PMC


