---
title: "openMobility Telco Sep 26th, 2023"
date: 2023-09-26T17:09:45+02:00
layout: "single"
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
---

## Attendees

- Robert Hilbrich (DLR)
- Martin Reuss (7P)
- Michael Behrisch (DLR)
- Evangelos Mintsis (CERTH/HIT)
- Dominik Salles (FKFS)
- Josko Balic (AVL)
- Alex Hainen (UA)

## Introduction

- Meeting minutes of the previous openMobility meeting have been accepted

## News from the Projects

### Eclipse MOSAIC

- no one of Eclipse MOSAIC present

### Eclipse openMCx

- Discussion about extending FMI 3.0 support for selected features
- Prototype implementation of DCP is available and potentially being added to OpenMCx

### Eclipse SUMO

- Source code repository moved to https://github.com/eclipse-sumo/sumo
- Website repository moved to https://github.com/eclipse-sumo/sumo.website
- Reason:
  - move source code into an organization for each project to have more computing power for GitHub Actions and get features like branch protection
  - have a Zenodo integration for a DOI for each software release
- Please change the URL of your git checkout
- Fixed: Maven artifacts jars are still missing for v1.18.0 (needed some manual triggering)
- Date for the next SUMO User Conference 2024: **May 13-15, 2024** - please mark the date in your calenders
- Next SUMO release on Oct. 10 with JuPedSim pedestrian dynamic models integration (early alpha!)

### Other News

- Max Schrader is continuing the dissertation with radar and sensor modeling
- Dominik Salles is part of AIAMO (https://aiamo.de/, https://bmdv.bund.de/SharedDocs/DE/Artikel/DG/KI-Projekte/aiamo.html) 
  - working towards digital twins of two cities
  - Gathering data on the vehicle side
- Martin Reuss is continuing the work on MobilityOps to build digital twins
  - Current project: microhubs for cargo bikes
  - Presentation for EclipseCon
- Evangelos Mintsis built a digital twin of Tessaloniki to assess the efficiency of new traffic signal plans

## Towards an Updated Vision of openMobility

- Proposed vision: Collaborate on tools for digital twins of cities and municipalites to enable more efficient traffic and mobility management solutions.
  - Feedback: none - Vision is accepted for now.

- Suggestion: do a survey about available models or tools resulting from EU projects?
  - Feedback: none

- Suggestion: invite stakeholder from cities or municipalities to the openMobility community?
  - Invite "SmartCity people" from Brunswick and Wolfsburg?
  - Provide incentives to collaborate with the cities or municipalities?

- Should we work on "standardized" workflows to create digital twins?
  - Positive feedback, but writing code is more fun than writing guidelines - see experiences gained from "Road2Simulation" (https://www.dlr.de/ts/en/desktopdefault.aspx/tabid-11648/20367_read-46771/)
  - A driver for these activities is needed - potentially 7P?

- Should we work on APIs to import data from other tools?
  - Feedback is positive - needs more elaboration and more details

- Goal: get a proper work plan for 2024  

## EclipseCon 2023

- EclipseCon 2023 - Ludwigsburg, Germany, October 16 - 19, 2023 - https://www.eclipsecon.org/2023

## Any other Business

- Next meeting
  - in physical presence at the EclipseCon
  - as a virtual meeting: Nov. 21st, 2023, 14:30 - 15:30 CET