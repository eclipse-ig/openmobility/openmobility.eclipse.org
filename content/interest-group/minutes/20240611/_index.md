---
title: "openMobility Telco Jun 11, 2024"
date: 2024-06-11T17:09:45+02:00
layout: "single"
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
---

## Attendees

- Robert Hilbrich (DLR & co4e GmbH)
- Michael Behrisch (DLR)
- Michele Segata (University of Trento)
- Andy Riexinger (Bosch)
- Evangelos Mintsis (CERTH/HIT)
- Josko Balic (AVL)

## Introduction

- Feedback for the [SUMO User Conference 2024](https://eclipse.dev/sumo/conference/)
- We might be able to get a YouTube channel for the SUMO User Conference recordings

## News from the Projects

### Eclipse MOSAIC

- DLR colleague D. Wesemeyer started his doctoral thesis on v2x messages for traffic managemement with digital twins and traffic simulation, development of new message formats, ...

### Eclipse openMCx

- promoting simulation standards like FMI
- fix part of AVL cloud simulation application
- Will contribute the DCP module

- See last Automotive Meeting:
  - work on a [landscape](https://docs.google.com/presentation/d/1RTgdy7bBsn88WCBmt-7agxVcF9Z8EGxNcdSQPDIWres/edit?usp=sharing)
  - See [Meeting minutes](https://gitlab.eclipse.org/groups/eclipse/automotive/-/wikis/home/Eclipse-Automotive-bringing-Automotive-together#meeting-june-3rd-2024)

### Eclipse SUMO

- Release news (1.20 in May 2024)
  - New vehicle classes added (e.g. urban air traffic, wheelchair, ...)
  - Direct geo-routing in SUMO (mapmatching can now be done in SUMO)
  - Introduction of the JuPedSim coupling and bugfixes
- SUMO decided to pause the development of the FMU due to lack of user demand
- Maven Repository for libsumo for Java - see https://github.com/eclipse-sumo/sumo/issues/7921 - potentially switch to GitHub instead of Eclipse CI infrastructure
- Good bye WebSockets for the WebWizard?
  - Reason: containerization of browsers in recent Linux distributions prevents local web sockets
  - Option: open a new port locally instead
- Raise awareness about having regular SUMO developer meetings for non-DLR users and developers - would you be interested to join?
  - Scheduled for every other week - please get in touch with us, if you would like to join

## Upcoming Projects

### MobilityOps

- Probably not coming due to the lack of funding

### TAPAS

- A viable alternative to be provided as an Eclipse project - instead of "just" a GitHub repository

## Current Developments

- MantleAPI - see https://gitlab.eclipse.org/eclipse/openpass/mantle-api
  - Potential use case for SUMO: realize swarm traffic definitions in OpenSCENARIO

- APIs to seamlessly feed data from open or proprietary databases to SUMO
  - Interest to tap into other platforms (e.g., TomTom, MDS from Open Mobility Foundation etc.)?
  - See also: https://github.com/DKSR-Data-Competence-for-Cities-Regions/DKSR-Open-UrbanPulse

- Status: Planned publication about the current state of demand generation in SUMO
  - Topic is still evolving: new developments in Bologna and there is also MobiTopp from Karlsruhe
  - Topics needs to be revisited incorporating the current developments
  - Work pauses here for the moment

## Current state of SESAM

- SESAM is making progress with a nicer simulation playback functionality
- Next steps: uploading of custom scenarios and scenario modification
- How to do a co-simulation in the cloud?

## Any other Business

- Eclipse OpenCodeExperience (OCX)
  - who is attending or giving a talk?
  - there seems some technical issues with the conference
  - meeting in person at the conference?

- Next meeting: Juli 16th, 2024, 14:30 - 15:30 (Berlin time)
