---
title: "Minutes"
date: 2025-02-18T16:09:45+02:00
layout: "single"
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
---

Meeting minutes:

- [Telco Feb 18, 2025](/interest-group/minutes/20250218)
- [Telco Dec 12, 2024](/interest-group/minutes/20241210)
- [Telco Oct 15, 2024](/interest-group/minutes/20241015)
- [Telco Jun 11, 2024](/interest-group/minutes/20240611)
- Telco Mar 12, 2024 - canceled
- [Telco Jan 23, 2024](/interest-group/minutes/20240123)
- [Telco Nov 21, 2023](/interest-group/minutes/20231121)
- [Telco Sep 26, 2023](/interest-group/minutes/20230926)
- [Telco Jul 11, 2023](/interest-group/minutes/20230711)
- [Telco Nov 15, 2022](/interest-group/minutes/20221115)
- [Telco Sep 06, 2022](/interest-group/minutes/20220906)
- [Telco Jun 14, 2022](/interest-group/minutes/20220614)
- [Telco Oct 19, 2021](/interest-group/minutes/20211019)
- [Telco Oct 05, 2021](/interest-group/minutes/20211005)
- [Telco Aug 10, 2021](/interest-group/minutes/20210810)

Older minutes:

- [Archive at Eclipse Wiki](https://wiki.eclipse.org/OpenMobilityMeetingMinutes)
