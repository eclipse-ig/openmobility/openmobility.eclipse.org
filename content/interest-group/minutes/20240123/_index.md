---
title: "openMobility Telco Jan 23, 2024"
date: 2024-01-23T17:09:45+02:00
layout: "single"
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
---

## Attendees

- Robert Hilbrich (DLR & co4e GmbH)
- Michael Behrisch (DLR)
- Dominik Salles (FKFS)
- Evangelos Mintsis (CERTH/HIT)
- Jakob Kaths (Vector)
- Moritz Schweppenhäuser (FhG FOKUS)
- Karl Schrab (FhG FOKUS)
- Tobias Lukowitz (7P)

## Introduction

- co4e GmbH joined the openMobility interest group
  - SESAM platform uses SUMO (see: https://sesam.co4e.com)

## News from the Projects

### Eclipse MOSAIC

- Received best paper award for paper about traffic state estimation at SimuTools
  - MOSAIC applications in the paper where also published on GitHub
- See: https://github.com/mosaic-addons/traffic-state-estimation

### Eclipse openMCx

- Josko not here

### Eclipse SUMO

- Google research: https://blog.research.google/2023/12/simulations-illuminate-path-to-post.html
- SUMO User Conference 2024
  - about 24 abstracts received
  - deadline for full paper is Feb 16th
- Jakob will return in February
- Next SUMO release will be in February
- Are there users requiring FMI 2.0 support in SUMO?
- A setup with openMCx would be a cool demo use case

## Upcoming Projects

### MobilityOps

- Discussion about architecture for a full-blown product for cities and municipalities incorporating SUMO, TAPAS, ...
  - Meeting at DLR in Berlin next week to continue this discussion
- Talk with Eclipse Foundation regarding the project proposal started
- Funding might require publishing of code on "open code platform" of the federal ministry of internals in Germany
- Cities and municipalities in Germany are more and more looking to use the masterportal (masterportal.org)
- Talk with DKSR about Open Urban Pulse will be next week - how to connect this to SUMO/TAPAS ecosystem

## Other News

- No actual news

## Work Plan 2024

- MantleAPI - see https://gitlab.eclipse.org/eclipse/openpass/mantle-api
  - DLR plans to implement the MantleAPI in SUMO
  - Benefit: get OpenSCENARIO 1.2 support via scenario engine from openPASS
  - EclipseCon 2024 talk about MantleAPI and OpenPASS, see: https://www.youtube.com/watch?v=qo7UmUtUzHA
  - Potential use case for SUMO: realize swarm traffic definitions in OpenSCENARIO

- APIs to seamlessly feed data from open or proprietary databases to SUMO
  - Interest to tap into other platforms (e.g., TomTom, MDS from Open Mobility Foundation etc.)?
  - See also: https://github.com/DKSR-Data-Competence-for-Cities-Regions/DKSR-Open-UrbanPulse
  - Last time: yes, we should proceed with working on this idea
  - How to proceed?
    - Implementation/development work in a European funded project with reporting in the interest group
  - Proposal: EU data act to standardize mobility data in Europe

- Ideas for the next EclipseCon a.k.a. "Open Code Experience" needed for next time

## Any other Business

- Next meeting
  - Zoom: March 12th, 2024, 14:30 - 15:30 CET
