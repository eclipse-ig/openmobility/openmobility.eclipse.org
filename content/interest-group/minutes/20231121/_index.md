---
title: "openMobility Telco Nov 21, 2023"
date: 2023-11-21T17:09:45+02:00
layout: "single"
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
---

## Attendees

- Robert Hilbrich (DLR)
- Michael Behrisch (DLR)
- Tobias Lukowitz (7P)
- Martin Reuss (7P)
- Michele Segata (University of Trento)
- Evangelos Mintsis (CERTH/HIT)
- Moritz Schweppenhäuser (FhG FOKUS)
- Robert Protzmann (FhG FOKUS)
- Dominik Salles (FKFS)
- Josko Balic (AVL)

## Introduction

- EclipseCon 2023 - impressions and feedback
  - Community day
    - Lots of talks from BMW
    - Interesting presentations from the folks from the Software Defined Vehicle (SDV) Working Group
    - SDV is getting a lot of attention within the automotive track - openMobility, openMDM and openPASS are a bit like a side-kick
    - Differentiation of Interest Groups and Working Groups was felt by participants
  - Conference
    - Positive feedback - range of topics is interesting

- Eclipse Meeting: "Bringing Automotive Together" organized by Andy Riexinger yesterday
  - Question: why does openMobility not move under the umbrella of SDV?
  - Too many groups? Too many projects?

## News from the Projects

### Eclipse MOSAIC

- New release in October
- See talk at EclipseCon about MOSAIC
- A publication about MOSAIC at Simutools conference

### Eclipse openMCx

- Requirements are going towards:
  - real-time co-simulation - "classical" simulation is maybe over?
  - DCP based simulation

### Eclipse SUMO

- New release 1.19.0
  - See changelog: https://sumo.dlr.de/docs/ChangeLog.html#version_1190_07112023
  - Integration of JuPedSim in sumo
  - Improvements in NetEdit to create scenarios in which pedestrians move between locations
  - Bidirectional movement improvements
  - The repository moved to https://github.com/eclipse-sumo/sumo and the website repo to https://github.com/eclipse-sumo/sumo.website
  - A user survey was announced and sent with the release notes

- SUMO User Conference 2024
  - May 13-15 in Berlin
  - See https://eclipse.dev/sumo/conference/ for the call for papers

## Upcoming Projects

### MobilityOps

- Decision to go open source has been made - but this is a first for SevenPrincipes
- Details have to be discussed and finalized
- Potential user base has been asked to assess the next steps as a validation
- A project application could be filled out - even if the contributor - the organization - is not an Eclipse member

## Other News

- DLR spin-off company co4e GmbH (co4e.com) joins the Eclipse Foundation and also openMobility

## Work Plan 2024

- APIs to seamlessly feed data from open or proprietary databases to SUMO
  - HIT/CERTH: NAPCORE project - feed data from the national access point to the simulator
  - Interest to tap into other platforms (e.g., TomTom, MDS from Open Mobility Foundation etc.)?
  - Problem of realistic scenarios is still not solved
  - Results would be beneficial to multiple projects
  - See also: https://github.com/DKSR-Data-Competence-for-Cities-Regions/DKSR-Open-UrbanPulse
  - Yes, we should proceed with working on this idea

- BluePrints on how to use "Eclipse Projects" - similar to Eclipse SDV Working Group
  - Support tool discovery phase on the group level
  - Tutorials exist already to describe the usage of our tools - also in conjunction with other tools; still they could be improved
  - Could be beneficial for cities? How does a "package" for a city look like? At least focus on the topic areas, where we can provide a contribution with our projects
  - Target audience: cities or new members for the interest group (traffic planners, companies in the field of mobility)
  - Yes, we should proceed with working on this idea
  - Goal: have sensible, helpful and practical blue prints

- Joint EU research project?
  - DLR is engaged in proposals for the calls HORIZON-CL5-2024-D6-01-11 and HORIZON-CL5-2024-D6-01-6
  - Please reach out to Robert or Michael, if you would like to collaborate in a HORIZON EU research project

## Any other Business

- Next meeting
  - Zoom: Jan. 23st, 2024, 14:30 - 15:30 CET