---
title: "openMobility Telco Jun 14th, 2022"
date: 2022-06-14T17:09:45+02:00
layout: "single"
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
---

## Attendees

- Robert Hilbrich
- Ronald Nippold
- Michael Behrisch
- Jakob Erdmann
- Michele Segata
- Robert Protzmann
- Jakob Kaths
- Dominik Salles
- Karl Schrab
- Michele Albano 

## News 

### Eclipse MOSAIC

- Last release of MOSAIC 2021 was last October, latest release is from May 2022
- New perception module in MOSAIC war presented at the SUMO User Conference
- Improvement of the libsumo integration which is working mostly
- Release version of libsumo / libtraci jars are still not working due to Eclipse CI Jenkins build (snapshot version are working for the moment)
- DLR will continue to try to fix this issue (the tag is not triggering the build process, maybe solve be switching to time triggered commits?)
- FhG FOKUS is working on a scenario for Berlin (demand taken from MATSim and duarouter, no public transport, no bikes, no peds - just passenger cars) - scaling of the "10% scenario" to a "100% scenario"
- Paper regarding the new scenario will be published soon
- Scenario will be published on soon as well

### Eclipse SUMO

- Three recent releases so far with a lot of new features
- NEMA traffic controllers are now supported (first contribution from partners from the US)
- A lot more features in netedit (trips, walking areas, ...)
- Improvements in the webwizard (added road type selection)
- Videos for the SUMO User Conference are online and to be found at YouTube (Eclipse Foundation channel)
- Proceedings for last year we are still waiting for the submission of two files - will happen next week (skipping authors who are not responding)
- Proceedings for this year will be happening during this summer break
- Next year will come with an established process to publish as a SCOPUS indexed conference series

### Eclipse Project Management Council (PMC) Automotive

- Andy Riexinger will be joining the Automotive PMC 
- New IP process at the Eclipse Foundation - not yet finalized

### Other News from the Attendees

- Plexe: Michele has been working on using multiple communication interfaces at the same time
- Vector: integration with SUMO has been improved and now makes use of generic parameters and SUMO integration is now part of the installer

## openMobility - Future of the "Working Group"

- Eclipse Foundation is working on a new concept for "working groups light"
- it will be introduced in the next couple of months
- Proposal: migrate openMobility to the new concept starting January 2023
- An official request for a "+1" from all steering committee working group members will follow the official announcement of the proposal by the Eclipse Foundation

## Quo vadis openADx?

- Andy Riexing will no longer be the chair of the openADx working group
- Idea: merging of openADx and openMobility starting January 2023?

## EclipseCon 2022

- EclipseCon 2022 will be happening as an event with physical presence see [EclipseCon website](https://www.eclipsecon.org/2022)
- Oct. 24-27 in Ludwigsburg, Germany
- EclipseCon features a specialized "Automotive and Mobility" track this year - but needs more submissions
- Deadline for submissions: 15.06.2022
- Community day on the first day of the EclipseCon

### Whitepaper: Survey or Landscape of Demand Generators for SUMO

- current status and progress?
- unfortunately not much progress so far
- Michele Albano will try to organize a meeting to discuss the next steps

## Future Ideas for openMobility?

- Robert Protzmann: scenario generation or standardized interfaces (e.g. openSCENARIO)
- Dominik Salles: continue to work on Github issues (openmobility-wg/userstories)
- more? send to mailinglist and discuss next time

## Any other Business

- openSCENARIO 2.0 is still not released and a lot of features have been left out 
- openSCENARIO 2.0 provides a public release candidate for everyone to have a look
