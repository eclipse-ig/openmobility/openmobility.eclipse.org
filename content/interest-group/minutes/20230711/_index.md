---
title: "openMobility Telco Jul 11th, 2023"
date: 2023-07-11T17:09:45+02:00
layout: "single"
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
---

## Attendees

- Robert Hilbrich (DLR)
- Michael Behrisch (DLR)
- Christoph Sommer (TU Dresden)
- Moritz Schweppenhäuser (FhG FOKUS)
- Tobias Lukowitz (7P)
- Robert Protzmann (FhG FOKUS)
- Karl Schrab (FhG FOKUS)
- Jakob Kaths (Vector)
- Josko Balic (AVL)
- Evangelos Mintsis (CERTH/HIT)

## Introduction

- Welcome to the openMobility interest group - the transition from the working group has been concluded successfully
- Advantages: less bureaucracy for everyone, less administrative overhead for us, join the group with a mouse click (no more signatures)
- Mailinglists and websites should continue to work for everybody who was already subscribed to the "old" openMobility mailinglist
- Meeting notes will be kept on the website: https://openMobility.eclipse.org/interest-group/minutes/

## News from the Projects

### Eclipse MOSAIC

- 2023.0 Release contains updates to the perception models and bugfixing
- Using train simulations in SUMO which may lead to new issues being posted on GitHub

### Eclipse SUMO

- SUMO v1.18.0 was released a couple of weeks ago
- Focus on integrating python tools in NetEdit (access python tools from within NetEdit)
- Updated emission model (HBEFA 4) - recommended to rerun the simulation scenarios
- Improvements on bidirectional traffic simulation towards shared space simulation
- Bugfixes
- Jakob will be on a sabbatical until February 2024
- Beware: emission values changed the unit (unfortunately without a change in the API version number)  
- Maven artifacts jars are still missing for v1.18.0

### Eclipse openMCx

- Interest for openMCx is rising
- Request for testing wave energy plants based on openMCx - leads to the need for new features
- Interest in near-real-time fmu-fmu coupling via openMCx
- Extensions to DCP and sensor testing are being worked on

## Towards an Updated Vision of openMobility

- Projects in our purview:
  - Eclipse SUMO - https://eclipse.dev/sumo/
  - Eclipse MOSAIC - https://www.eclipse.org/mosaic/
  - Eclipse openMCx - https://projects.eclipse.org/projects/automotive.openmcx
  - (Eclipse Adore - https://projects.eclipse.org/projects/automotive.adore - a new release based on ROS2 will be available in the Q4/2023 - could move to Eclipse SDV?)

- Potential new projects:
  - TAPAS and TAPAS-SUMO-Coupling - https://github.com/DLR-VF/TAPAS, https://github.com/DLR-TS/TSC
  - MobilityOps (7P) - https://strazoon.com/planning-solutions/

- Collaborate on tools to build digital twins for cities and municipalites to tackle traffic and mobility management?
- Within openMobility the available tools range from traffic demand generation to traffic simulation, ITS applications and visualization for building mobility management strategies
- Political opportunity and market opportunity for open source tools due to the software procurement restrictions of the smart city model projects in Germany
- Cities need evidence based analysis and impact analysis as a prerequisite for acquiring more public funds (...)
- Suggestion: do a survey about available models or tools resulting from EU projects?
- Suggestion: invite stakeholder from cities or municipalities to the openMobility community?

## EclipseCon 2023

- EclipseCon 2023 - Ludwigsburg, Germany, October 16 - 19, 2023 - https://www.eclipsecon.org/2023
- Automotive and Mobility track: https://www.eclipsecon.org/session-tracks-eclipsecon-2023/automotive-mobility
- Accepted presentations:
   - https://www.eclipsecon.org/2023/sessions/leveraging-eclipse-sumo-and-eclipse-mosaic-unleashing-power-digital-twins-efficient
   - https://www.eclipsecon.org/2023/sessions/create-detailed-traffic-models-and-simulations-open-source-tools-and-open-data-blink
- Community day October 16th, 2023, 08:30 - 18:30 CEST
   - Draft agenda: https://wiki.eclipse.org/EclipseCon_2023_Automotive_Community_Day

## Any other Business

- Chat client changes: mattermost has been terminated, the SUMO channel moved to the new chat service at Eclipse (https://chat.eclipse.org/#/welcome)
- Do we want to have a chat channel on chat.eclipse.org for openMobility? (only public channels are allowed)
- Complexity of the initiatives at Eclipse are hard to understand
  - Automotive Top Level Project,
  - Eclipse Projects,
  - Eclipse Project Management Council,
  - "Bringing Automotive together",
  - openMobility IG,
  - Working Groups, ...
- Please join the interest group, if you are already part of the Eclipse Foundation ecosystem (https://openmobility.eclipse.org/interest-group/become-a-member/)
- Next meeting: Tuesday, September 26th, 14:30 CEST via Zoom